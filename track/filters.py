from .models import Task, Event
import django_filters


class UserTasksFilter(django_filters.FilterSet):
    class Meta:
        model = Task
        fields = {
            'user': ['exact'],
            'title': ['icontains'],
            'status': ['exact'],
            'deadline_date': ['exact']
        }


class DateTasksFilter(django_filters.FilterSet):
    class Meta:
        model = Task
        fields = {
            'deadline_date': ['exact'],
            'status': ['exact']
        }


class UserEventsFilter(django_filters.FilterSet):
    class Meta:
        model = Event
        fields = {
            'user': ['exact'],
            'title': ['icontains'],
            'is_planned': ['exact']
        }


class UserPlanEventsFilter(django_filters.FilterSet):
    date = django_filters.DateFilter(name='date', label='First date', lookup_expr='exact')

    class Meta:
        model = Event
        fields = {
            'user': ['exact'],
            'title': ['icontains'],
            'date': ['exact']
        }
