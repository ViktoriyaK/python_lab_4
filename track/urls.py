from django.conf.urls import url
from . import views
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required


urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='start.html'), name='start'),
    url(r'^today/$', views.today, name='today_list'),

    url(r'^tasks/$', views.tasks_filter, name='tasks_filter'),
    url(r'^tasks/new/$', views.task_new, name='task_new'),
    url(r'^tasks/(?P<pk>\d+)/$', views.task_detail, name='task_detail'),
    url(r'^tasks/(?P<pk>\d+)/edit/$', views.task_edit, name='task_edit'),
    url(r'^tasks/(?P<pk>\d+)/done/$', views.task_done, name='task_done'),
    url(r'^tasks/(?P<pk>\d+)/delete/$', login_required(views.DeleteTaskView.as_view()), name='task_delete'),
    url(r'^tasks/(?P<pk>\d+)/new_subtask', views.subtask_new, name='subtask_new'),
    url(r'^tasks/groups/$', views.tasks_groups, name='tasks_groups'),
    url(r'^tasks/groups/(?P<pk>\d+)/$', views.tasks_group, name='tasks_group'),

    url(r'^events/$', views.events_filter, name='events_filter'),
    url(r'^events/new/$', views.event_new, name='event_new'),
    url(r'^events/(?P<pk>\d+)/$', views.event_detail, name='event_detail'),
    url(r'^events/(?P<pk>\d+)/edit/$', views.event_edit, name='event_edit'),
    url(r'^events/(?P<pk>\d+)/delete/$', login_required(views.DeleteEventView.as_view()), name='event_delete'),
    url(r'^events/groups/$', views.events_groups, name='events_groups'),
    url(r'^events/groups/(?P<pk>\d+)/$', views.events_group, name='events_group'),
    url(r'^events/plans$', views.plans_filter, name='plans_filter'),
    url(r'^events/plans/new_plan$', views.event_plan_new, name='event_plan_new'),
    url(r'^events/plans/new_plan_days/$', views.event_plan_days_new, name='event_plan_days_new'),
    url(r'^events/plans/(?P<event_pk>\d+)/edit/$', views.event_plan_edit, name='event_plan_edit'),

    url(r'^groups/$', views.group_new, name='group_new'),
]
