from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from .models import (
    Task,
    Event,
    Group,
)
from .filters import (
    UserTasksFilter,
    UserEventsFilter,
    DateTasksFilter,
    UserPlanEventsFilter,
)
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login, authenticate
from .forms import (
    AddTaskForm,
    EditTaskForm,
    AddEventForm,
    EditEventForm,
    DoneTaskForm,
    AddEventPlanForm,
    AddEventPlanDaysForm,
    EditEventPlanForm,
    EditEventPlanDaysForm,
    AddGroupForm,
    DateForm,
)
from django.views.generic import DeleteView
from django.urls import reverse
from django.db.models import Q
from django.core.exceptions import PermissionDenied


@login_required
def today(request):
    tasks = Task.objects.filter(user=request.user, status="a", deadline_date=timezone.now().date(),
                                parent_task__isnull=True).order_by('priority')
    events = Event.objects.filter(user=request.user, date=timezone.now().date(), is_planned=False).order_by('time')
    tasks_without_deadline = Task.objects.filter(user=request.user, status="a", parent_task__isnull=True,
                                                 deadline_date__isnull=True).order_by('priority')

    planned = Event.objects.filter(user=request.user, is_planned=True)
    planned_event = _get_today_planned_events(planned)

    return render(request, 'track/today.html',
                  {
                      'tasks': tasks,
                      'events': events,
                      'plans': planned_event,
                      'tasks_without_deadline': tasks_without_deadline,
                      'failed_tasks': update_status(request.user)
                  })


def _get_today_planned_events(planned_events, date=None):
    """Returns planned events if they exist on that date."""
    if date is None:
        date = timezone.now().date()
    today_planned_events = []
    for event in planned_events:
        # event can contain plan with relative delta field or with lists of days
        plan = event.plan if event.plan else event.plan_days
        if plan.is_today(date):
            today_planned_events.append(event)
    return today_planned_events


@login_required
def task_detail(request, pk):
    task = get_object_or_404(Task, pk=pk)
    return render(request, 'track/task_details.html',
                  {
                      'task': task,
                      'failed_tasks': update_status(request.user),
                  })


@login_required
def event_detail(request, pk):
    event = get_object_or_404(Event, pk=pk)
    return render(request, 'track/event_details.html', {'event': event})


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('start')
    else:
        form = UserCreationForm()
    return render(request, 'registration/signup.html', {'form': form})


@login_required
def task_new(request):
    if request.method == "POST":
        form = AddTaskForm(request.user, request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            task.user = request.user
            task.create_datetime = timezone.now()
            task.edit_datetime = task.create_datetime
            task.save()
            task.group.add(*form.cleaned_data['group'])
            return redirect('today_list')
    else:
        form = AddTaskForm(user=request.user)
    return render(request, 'track/task_new.html', {'form': form})


@login_required
def subtask_new(request, pk):
    if request.method == "POST":
        form = AddTaskForm(request.user, request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            task.user = request.user
            task.create_datetime = timezone.now()
            task.edit_datetime = task.create_datetime
            task.parent_task_id = pk
            task.save()
            task.group.add(*form.cleaned_data['group'])
            return redirect('today_list')
    else:
        form = AddTaskForm(request.user)
    return render(request, 'track/task_new.html', {'form': form})


@login_required
def task_edit(request, pk):
    task = get_object_or_404(Task, pk=pk)
    if not (task.user == request.user or task.access == "w"):
        raise PermissionDenied
    if request.method == "POST":
        form = EditTaskForm(task.user, request.POST, instance=task)
        if form.is_valid():
            task = form.save(commit=False)
            task.edit_datetime = timezone.now()
            task.save()
            task.group.clear()
            task.group.add(*form.cleaned_data['group'])
            return redirect('task_detail', pk=task.pk)
    else:
        form = EditTaskForm(instance=task, user=task.user)
    return render(request, 'track/task_edit.html',
                  {'form': form, 'task': task, 'failed_tasks': update_status(request.user), 'task_pk': task.pk})


@login_required
def event_new(request):
    if request.method == "POST":
        form = AddEventForm(request.user, request.POST)
        if form.is_valid():
            _save_event(form, request.user)
            return redirect('today_list')
    else:
        form = AddEventForm(request.user)
    return render(request, 'track/event_new.html', {'form': form})


@login_required
def event_edit(request, pk):
    event = get_object_or_404(Event, pk=pk)
    if not (event.user == request.user or event.access == "w"):
        raise PermissionDenied
    if request.method == "POST":
        form = EditEventForm(event.user, request.POST, instance=event)
        if form.is_valid():
            _save_event(form)
            return redirect('event_detail', pk=event.pk)
    else:
        form = EditEventForm(instance=event, user=event.user)
    return render(request, 'track/event_edit.html', {'event_form': form, 'event_pk': event.pk})


def _save_event(form, user=None):
    event = form.save(commit=False)
    groups = form.cleaned_data['group']
    event.edit_datetime = timezone.now()
    if user:
        event.user = user
    event.save()
    event.group.clear()
    event.group.add(*groups)
    return event


def _save_new_plan(request, form, date):
    plan = form.save(commit=False)
    plan.user = request.user
    plan.first_date = date
    plan.save()
    return plan


@login_required
def event_plan_new(request):
    if request.method == "POST":
        event_form = AddEventForm(request.user, request.POST)
        plan_form = AddEventPlanForm(request.POST)
        if event_form.is_valid() and plan_form.is_valid():
            event = _save_event(event_form, request.user)
            event.is_planned = True

            plan = _save_new_plan(request, plan_form, event.date)

            event.plan = plan
            event.save()
            return redirect('today_list')
    else:
        event_form = AddEventForm(request.user)
        plan_form = AddEventPlanForm()
    return render(request, 'track/event_plan_new.html', {'event_form': event_form, 'plan_form': plan_form})


@login_required
def event_plan_days_new(request):
    if request.method == "POST":
        event_form = AddEventForm(request.user, request.POST)
        plan_form = AddEventPlanDaysForm(request.POST)
        if event_form.is_valid() and plan_form.is_valid():
            event = _save_event(event_form, request.user)
            event.is_planned = True

            plan = _save_new_plan(request, plan_form, event.date)

            event.plan_days = plan
            event.save()
            return redirect('today_list')
    else:
        event_form = AddEventForm(request.user)
        plan_form = AddEventPlanDaysForm()
    return render(request, 'track/event_plan_new.html', {'event_form': event_form, 'plan_form': plan_form})


@login_required
def event_plan_edit(request, event_pk):
    event = get_object_or_404(Event, pk=event_pk)
    if not (event.user == request.user or event.access == "w"):
        raise PermissionDenied
    if event.plan:
        plan = event.plan
        form_class = EditEventPlanForm
    else:
        plan = event.plan_days
        form_class = EditEventPlanDaysForm
    if request.method == "POST":
        form = form_class(request.POST, instance=plan)
        if form.is_valid():
            plan = form.save(commit=False)
            plan.edit_datetime = timezone.now()
            form.save()
            return redirect('event_detail', pk=event_pk)
    else:
        form = form_class(instance=plan)
    return render(request, 'track/plan_edit.html', {'form': form, 'event_pk': event_pk})


class DeleteTaskView(DeleteView):
    template_name = 'task_delete.html'
    model = Task

    def get_object(self, **kwargs):
        obj = super(DeleteTaskView, self).get_object()
        if not obj.user == self.request.user:
            raise PermissionDenied
        return obj

    def get_success_url(self):
        return reverse('today_list')


@login_required
def task_done(request, pk):
    task = get_object_or_404(Task, pk=pk)
    if task.user != request.user:
        raise PermissionDenied
    if request.method == "POST":
        form = DoneTaskForm(request.POST, instance=task)
        if form.is_valid():
            task = form.save(commit=False)
            task.done()
            return redirect('task_detail', pk=task.pk)
    else:
        form = DoneTaskForm(instance=task)
    return render(request, 'track/task_done.html', {'form': form, 'failed_tasks': update_status(request.user)})


class DeleteEventView(DeleteView):
    template_name = 'event_delete.html'
    model = Event

    def get_object(self, **kwargs):
        obj = super(DeleteEventView, self).get_object()
        if not obj.user == self.request.user:
            raise PermissionDenied
        return obj

    def get_success_url(self):
        return reverse('today_list')


@login_required
def tasks_filter(request):
    f = UserTasksFilter(request.GET, queryset=Task.objects.filter(
        Q(user=request.user) | (~Q(user=request.user) & (Q(access="r") | Q(access="w")))
    ))
    return render(request, 'track/tasks_filter.html', {'filter': f, 'failed_tasks': update_status(request.user)})


@login_required
def events_filter(request):
    queryset = (
        Event.objects.filter(user=request.user) |
        Event.objects.filter(access="r") |
        Event.objects.filter(access="w")
    ).order_by('time')

    date_form = DateForm(request.GET)
    if date_form.is_valid():
        date = date_form.cleaned_data['date']
        events = queryset.filter(date=date, is_planned=False)
        planned_events = queryset.filter(is_planned=True)
        planned_date_events_ids = [e.id for e in _get_today_planned_events(planned_events, date)]
        planned_date_events = Event.objects.filter(id__in=planned_date_events_ids)
        queryset = (events | planned_date_events).order_by('time')

    f = UserEventsFilter(request.GET, queryset=queryset)
    return render(request, 'track/events_filter.html', {'filter': f, 'date_form': date_form})


@login_required
def plans_filter(request):
    f = UserPlanEventsFilter(request.GET, queryset=Event.objects.filter(Q(is_planned=True) & (
        Q(user=request.user) | (~Q(user=request.user) & (Q(access="r") | Q(access="w"))))
    ))
    return render(request, 'track/plans_filter.html', {'filter': f})


@login_required
def group_new(request):
    groups = Group.objects.filter(user=request.user)
    if request.method == "POST":
        form = AddGroupForm(request.user, request.POST)
        if form.is_valid():
            group = form.save(commit=False)
            group.user = request.user
            group.save()
            return redirect('group_new')
    else:
        form = AddGroupForm(user=request.user)
    return render(request, 'track/group_new.html', {'form': form, 'groups': groups})


@login_required
def tasks_groups(request):
    groups = Group.objects.filter(task__user=request.user).distinct()
    return render(request, 'track/task_group_selection.html',
                  {'groups': groups, 'failed_tasks': update_status(request.user)})


@login_required
def tasks_group(request, pk):
    f = DateTasksFilter(request.GET, queryset=Task.objects.filter(user=request.user, group__pk=pk))
    return render(request, 'track/tasks_filter.html', {'filter': f})


@login_required
def events_groups(request):
    groups = Group.objects.filter(event__user=request.user).distinct()
    return render(request, 'track/event_group_selection.html', {'groups': groups})


@login_required
def events_group(request, pk):
    events = Event.objects.filter(user=request.user, group__pk=pk, is_planned=False).order_by('time')
    planned_events = Event.objects.filter(user=request.user, group__pk=pk, is_planned=True)
    date_form = DateForm(request.GET)
    if date_form.is_valid():
        date = date_form.cleaned_data['date']
        events = events.filter(date=date)
        planned_events = _get_today_planned_events(planned_events, date=date)
        events = sorted(list(events) + planned_events, key=lambda event: event.time)
    else:
        events = (events | planned_events).order_by('time')
    return render(request, "track/group_events_filter.html", {
        "events": events,
        "date_form": date_form
    })


def update_status(user):
    failed_titles = Task.update_status(user)
    if failed_titles:
        return ", ".join(failed_titles)
    return None
