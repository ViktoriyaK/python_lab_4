from django import forms
from django.core.exceptions import ValidationError

from .models import Task, Event, EventPlan, EventPlanDays, Group


class AddTaskForm(forms.ModelForm):
    def __init__(self, user, *args, **kwargs):
        super(AddTaskForm, self).__init__(*args, **kwargs)
        self.fields['group'].queryset = Group.objects.filter(user=user)

    class Meta:
        model = Task
        fields = ('title', 'deadline_date', 'priority', 'group', 'description', )
        widgets = {
            'title': forms.widgets.TextInput(attrs={'class': 'form-control', 'style': 'margin-bottom: 15px'}),
            'deadline_date': forms.widgets.DateInput(
                attrs={'type': 'date', 'class': 'form-control', 'style': 'margin-bottom: 15px'}),
            'priority': forms.widgets.Select(attrs={'class': 'form-control', 'style': 'margin-bottom: 15px'}),
            'group': forms.widgets.CheckboxSelectMultiple(attrs={'style': 'margin-bottom: 15px'}),
            'description': forms.Textarea(
                attrs={'cols': 30, 'rows': 3, 'class': 'form-control', 'style': 'margin-bottom: 15px'})
        }


class EditTaskForm(forms.ModelForm):
    def __init__(self, user, *args, **kwargs):
        super(EditTaskForm, self).__init__(*args, **kwargs)
        self.fields['group'].queryset = Group.objects.filter(user=user)

    class Meta:
        model = Task
        fields = ('title', 'deadline_date', 'priority', 'description',
                  'related_tasks', 'group', 'access',)
        widgets = {
            'title': forms.widgets.TextInput(attrs={'class': 'form-control', 'style': 'margin-bottom: 15px'}),
            'deadline_date': forms.widgets.DateInput(
                attrs={'type': 'date', 'class': 'form-control', 'style': 'margin-bottom: 15px'}),
            'priority': forms.widgets.Select(attrs={'class': 'form-control', 'style': 'margin-bottom: 15px'}),
            'access': forms.widgets.Select(attrs={'class': 'form-control', 'style': 'margin-bottom: 15px'}),
            'group': forms.widgets.CheckboxSelectMultiple(attrs={'style': 'margin-bottom: 15px'}),
            'description': forms.Textarea(
                attrs={'cols': 30, 'rows': 3, 'class': 'form-control', 'style': 'margin-bottom: 15px'}),
            'related_tasks': forms.Textarea(
                attrs={'cols': 30, 'rows': 3, 'class': 'form-control', 'style': 'margin-bottom: 15px'})
        }


class DoneTaskForm(forms.ModelForm):

    class Meta:
        model = Task
        fields = tuple()


class AddEventForm(forms.ModelForm):
    def __init__(self, user, *args, **kwargs):
        super(AddEventForm, self).__init__(*args, **kwargs)
        self.fields['group'].queryset = Group.objects.filter(user=user)

    class Meta:
        model = Event
        fields = ('title', 'date', 'time', 'priority', 'group', 'description', )
        widgets = {
            'title': forms.widgets.TextInput(attrs={'class': 'form-control', 'style': 'margin-bottom: 15px'}),
            'date': forms.widgets.DateInput(
                attrs={'type': 'date', 'class': 'form-control', 'style': 'margin-bottom: 15px'}),
            'time': forms.widgets.TimeInput(
                attrs={'type': 'time', 'class': 'form-control', 'style': 'margin-bottom: 15px'}),
            'priority': forms.widgets.Select(attrs={'class': 'form-control', 'style': 'margin-bottom: 15px'}),
            'group': forms.widgets.CheckboxSelectMultiple(attrs={'style': 'margin-bottom: 15px'}),
            'description': forms.Textarea(
                attrs={'cols': 30, 'rows': 3, 'class': 'form-control', 'style': 'margin-bottom: 15px'})
        }


class EditEventForm(forms.ModelForm):
    def __init__(self, user, *args, **kwargs):
        super(EditEventForm, self).__init__(*args, **kwargs)
        self.fields['group'].queryset = Group.objects.filter(user=user)

    class Meta:
        model = Event
        fields = ('title', 'date', 'time', 'priority', 'description', 'group', 'access',)
        widgets = {
            'title': forms.widgets.TextInput(attrs={'class': 'form-control', 'style': 'margin-bottom: 15px'}),
            'date': forms.widgets.DateInput(
                attrs={'type': 'date', 'class': 'form-control', 'style': 'margin-bottom: 15px'}),
            'time': forms.widgets.TimeInput(
                attrs={'type': 'time', 'class': 'form-control', 'style': 'margin-bottom: 15px'}),
            'priority': forms.widgets.Select(attrs={'class': 'form-control', 'style': 'margin-bottom: 15px'}),
            'access': forms.widgets.Select(attrs={'class': 'form-control', 'style': 'margin-bottom: 15px'}),
            'group': forms.widgets.CheckboxSelectMultiple(attrs={'style': 'margin-bottom: 15px'}),
            'description': forms.Textarea(
                attrs={'cols': 30, 'rows': 3, 'class': 'form-control', 'style': 'margin-bottom: 15px'})
        }


class AddEventPlanForm(forms.ModelForm):
    def clean(self):
        cleaned_data = super().clean()
        days = cleaned_data.get("days")
        months = cleaned_data.get("months")
        years = cleaned_data.get("years")
        if days == 0 and months == 0 and years == 0:
            raise ValidationError("At least one of the fields must be different from 0.")

    class Meta:
        model = EventPlan
        fields = ('days', 'months', 'years')
        widgets = {
            'days': forms.widgets.NumberInput(attrs={'class': 'form-control', 'style': 'margin-bottom: 15px'}),
            'months': forms.widgets.NumberInput(attrs={'class': 'form-control', 'style': 'margin-bottom: 15px'}),
            'years': forms.widgets.NumberInput(attrs={'class': 'form-control', 'style': 'margin-bottom: 15px'}),
        }


class AddEventPlanDaysForm(forms.ModelForm):
    class Meta:
        model = EventPlanDays
        fields = ('days',)
        widgets = {'days': forms.widgets.CheckboxSelectMultiple(attrs={'style': 'margin-bottom: 15px'})}


class EditEventPlanForm(forms.ModelForm):

    class Meta:
        model = EventPlan
        fields = ('days', 'months', 'years')
        widgets = {
            'days': forms.widgets.NumberInput(attrs={'class': 'form-control', 'style': 'margin-bottom: 15px'}),
            'months': forms.widgets.NumberInput(attrs={'class': 'form-control', 'style': 'margin-bottom: 15px'}),
            'years': forms.widgets.NumberInput(attrs={'class': 'form-control', 'style': 'margin-bottom: 15px'}),
        }


class EditEventPlanDaysForm(forms.ModelForm):

    class Meta:
        model = EventPlanDays
        fields = ('days', )
        widgets = {
            'days': forms.widgets.CheckboxSelectMultiple(attrs={'style': 'margin-bottom: 15px'}),
        }


class AddGroupForm(forms.ModelForm):
    def __init__(self, user, *args, **kwargs):
        super(AddGroupForm, self).__init__(*args, **kwargs)
        self.user = user

    def clean(self):
        cleaned_data = super().clean()
        name = cleaned_data.get("name")
        if Group.objects.filter(user=self.user, name=name).exists():
            raise ValidationError("Group with that name already exists.")

    class Meta:
        model = Group
        fields = ('name',)
        widgets = {
            'name': forms.TextInput()
        }


class DateForm(forms.Form):
    date = forms.DateField(required=False)
