from django.contrib import admin
from .models import Task, Group, Event, EventPlan, EventPlanDays

admin.site.register(Task)
admin.site.register(Group)
admin.site.register(Event)
admin.site.register(EventPlan)
admin.site.register(EventPlanDays)
