from django.db import models
from django.conf import settings
from django.utils import timezone
from multiselectfield import MultiSelectField
from dateutil.relativedelta import relativedelta


class Group(models.Model):
    name = models.CharField(max_length=30)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)


class Task(models.Model):
    """Implements Task table."""
    title = models.CharField(max_length=50)
    create_datetime = models.DateTimeField(default=timezone.now)
    deadline_date = models.DateField(null=True, blank=True)
    priority = models.IntegerField(default=0, choices=(
        (0, "No"),
        (1, "Low"),
        (2, "Medium"),
        (3, "High")
    ))
    group = models.ManyToManyField(Group, blank=True, default=None)
    description = models.TextField(blank=True)
    access = models.CharField(max_length=1, default="p", choices=(
        ("p", "Private"),
        ("r", "Read"),
        ("w", "Write")
    ))
    is_ready = models.BooleanField(default=False)
    status = models.CharField(max_length=1, default="a", choices=(
        ("a", "Active"),
        ("r", "Ready"),
        ("f", "Failed"),
    ))
    related_tasks = models.TextField(blank=True)
    parent_task = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    edit_datetime = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.title

    def get_subtasks(self):
        return Task.objects.filter(parent_task__id=self.id)

    def get_status_style_color(self):
        colors = {"a": "black", "r": "green", "f": "red"}
        return colors[self.status]

    def done(self):
        self._recursive_done_down()
        self._recursive_done_up()

    def _mark_ready(self):
        self.status = "r"
        self.edit_datetime = timezone.now()
        self.save()

    def _recursive_done_down(self):
        self._mark_ready()
        subs = Task.objects.filter(parent_task__id=self.id)
        for sub in subs:
            sub._recursive_done_down()

    def _recursive_done_up(self):
        if self.parent_task:
            parent = self.parent_task
            if (Task.objects.filter(parent_task__id=parent.id, status="r").count() ==
                Task.objects.filter(parent_task__id=parent.id).count()):
                parent._mark_ready()
                parent._recursive_done_up()

    def _mark_failed(self):
        self.status = "f"
        self.edit_datetime = timezone.now()
        self.save()

    def update_status(user):
        failed_titles = []
        failed_tasks = Task.objects.filter(user__id=user.id, status="a", deadline_date__lt=timezone.now().date(),
                                           parent_task__isnull=True)

        parents = []
        for task in failed_tasks:
            failed_titles.append(task.title)
            task._mark_failed()
            if task.parent_task:
                parents.append(task.parent_task)
            failed_titles.extend(task._update_subs_status(user))
        failed_titles.extend(Task._update_parent_tasks_status(parents))
        return failed_titles

    def _update_subs_status(self, user):
        subs = Task.objects.filter(user__id=user.id, status="a", parent_task__id=self.id)
        failed_titles = []
        for sub in subs:
            failed_titles.append(sub.title)
            sub._mark_failed()
            failed_titles.extend(sub._update_subs_status(user))
        return failed_titles

    @staticmethod
    def _update_parent_tasks_status(parents):
        failed_titles = []
        new_parents = []
        for parent in parents:
            if parent.status == "f":
                continue
            if (Task.objects.filter(parent_task__id=parent.id, status="f").count() ==
                    Task.objects.filter(parent_task__id=parent.id).count()):
                failed_titles.append(parent.title)
                parent._mark_failed()
                if parent.parent_task:
                    new_parents.append(parent.parent_task)
        if new_parents:
            failed_titles.extend(Task._update_parent_tasks_status(new_parents))
        return failed_titles


class EventPlan(models.Model):
    """Implements EventPlan table."""
    years = models.PositiveSmallIntegerField(null=True, blank=True, default=0)
    months = models.PositiveSmallIntegerField(null=True, blank=True, default=0)
    days = models.PositiveSmallIntegerField(null=True, blank=True, default=1)
    first_date = models.DateField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    edit_datetime = models.DateTimeField(default=timezone.now)

    def is_today(self, date):
        delta_date = relativedelta(days=self.days, months=self.months, years=self.years)
        if date < self.first_date:
            return False
        temp_date = self.first_date
        while temp_date < date:
            temp_date += delta_date
        if temp_date == date:
            return True
        return False


class EventPlanDays(models.Model):
    """Implements EventPlanDays table."""
    days = MultiSelectField(choices=(
        (0, "Monday"),
        (1, "Tuesday"),
        (2, "Wednesday"),
        (3, "Thursday"),
        (4, "Friday"),
        (5, "Saturday"),
        (6, "Sunday")
    ))
    first_date = models.DateField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    edit_datetime = models.DateTimeField(default=timezone.now)

    def is_today(self, date):
        DAYS = {
            "Monday": 0,
            "Tuesday": 1,
            "Wednesday": 2,
            "Thursday": 3,
            "Friday": 4,
            "Saturday": 5,
            "Sunday": 6
        }
        if date < self.first_date:
            return False
        repeat_days = [DAYS[d] for d in self.get_days_list()]
        if date.weekday() in repeat_days:
            return True
        return False


class Event(models.Model):
    """Implements Event table."""
    title = models.CharField(max_length=50)
    create_datetime = models.DateTimeField(default=timezone.now)
    date = models.DateField()
    time = models.TimeField()
    priority = models.IntegerField(default=0, choices=(
        (0, "No"),
        (1, "Low"),
        (2, "Medium"),
        (3, "High")
    ))
    group = models.ManyToManyField(Group, blank=True, default=None)
    description = models.TextField(blank=True)
    access = models.CharField(max_length=1, default="p", choices=(
        ("p", "Private"),
        ("r", "Read"),
        ("w", "Write")
    ))
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    edit_datetime = models.DateTimeField(default=timezone.now)
    is_planned = models.BooleanField(default=False)
    plan = models.ForeignKey(EventPlan, on_delete=models.CASCADE, null=True, blank=True)
    plan_days = models.ForeignKey(EventPlanDays, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.title

    def get_plan_first_date(self):
        if not self.is_planned:
            return

        if self.plan:
            return self.plan.first_date
        else:
            return self.plan_days.first_date

    def get_plan_period(self):
        if not self.is_planned:
            return

        if self.plan:
            period = []
            if self.plan.days:
                period.append("{} day(s)".format(self.plan.days))
            if self.plan.months:
                period.append("{} month(s)".format(self.plan.months))
            if self.plan.years:
                period.append("{} year(s)".format(self.plan.years))
            return ",".join(period)
        else:
            return self.plan_days.get_days_display()
